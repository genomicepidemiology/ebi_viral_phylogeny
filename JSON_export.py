#!/usr/bin/env python3

import argparse
from copy import deepcopy
import os
import json
import sys
from typing import Dict
import sqlite3
import time
import itertools
import gzip
from pathlib import Path
from jsonschema import validate
from jsonschema.exceptions import ValidationError

# local imports
import viral_surveillance.vu_service as vu_service
from post_procesing.update_projects import load_projects
from post_procesing import load_metadata, load_newick

schema = {
    "definitions": {
        "analysis": {
            "type": "object",
            "properties":
            {
                "analysis_key": {"type": "string"},
                "title": {"type": "string"},
                "data_path": {"type": "string"},
                "custom_url": {"type": "string"}
            },
            "required": ["analysis_key"]
        }
    },
    "type": "object",
    "properties": {
        "project_key": {"type": "string"},
        "database_path": {"type": "string"},
        "mode": {"type": "string", "enum": ["ml", "dist"]},
        "export_root": {"type": "string"},
        "templates": {
            "type": "object",
            "additionalProperties": {"$ref": "#/definitions/analysis"}
        }
    },
    "required": ["project_key", "database_path", "templates"]
}


class AnalysisDetails:
    def __init__(self, analysis_key: str, title: str = None, data_path: str = None, custom_url: str = None):
        self.analysis_key = analysis_key
        self.data_path = data_path
        self.custom_url = custom_url
        self.title = title


class Config:
    def __init__(self, project_key: str, database_path: str, export_root: str, templates: Dict[str, AnalysisDetails] = None, method: str = 'dist',):
        self.project = project_key
        self.database = database_path
        self.export_root = export_root
        self.templates = templates
        self.method = method


def loc_arg_check(param):
    """Check argument with file path for file existence"""
    if param is not None and (
        os.path.exists(param) or os.path.exists("{}.name".format(param))
    ):
        return True
    else:
        return False


def validate_config(config_path: str) -> Config:
    if not loc_arg_check(config_path):
        vu_service.exiting("config needed")
    with open(config_path, 'r') as config_file:
        config: dict = json.load(config_file)

    try:
        validate(config, schema)
    except ValidationError:
        vu_service.exiting("configuration file has incorrect structure")

    # config: DefaultDict[str, any] = defaultdict({'analyses': {}}).update(config)

    database = config['database_path']

    if not loc_arg_check(database):
        vu_service.exiting(f"database does not exists on path {database}")

    if config['export_root'] is None:
        vu_service.exiting("Export destination not given")

    if not os.path.isdir(config['export_root']):
        vu_service.exiting("Export destination is not directory")

    analyses = {template: AnalysisDetails(
        **analysisDetails) for template, analysisDetails in config['templates'].items()}

    del config['templates']

    config = Config(**config, templates=analyses)

    return config


def addVariants(metadata: dict, cursor: sqlite3.Cursor):
    variatns = dict()
    cursor.execute('select accession,aa_variants from variants;')
    rows = cursor.fetchall()
    for (accession, aa_variants_raw) in rows:
        aa_variants = aa_variants_raw.split(
            ',') if aa_variants_raw != None else None
        variatns[accession] = aa_variants

    metadata_with_variants = deepcopy(metadata)
    for accesion in metadata.keys():
        if accesion in variatns:
            metadata_with_variants[accesion]['variants'] = variatns[accesion]

    return metadata_with_variants


def createAnalysis(data_hub: str, template: str, title: str, data_path: str = None, custom_url: str = None):
    analysis = {
        "data_hub": data_hub,
        "template": f'{template}',
        "title": title,
    }
    if data_path is not None:
        analysis['data_path'] = data_path
    if custom_url is not None:
        analysis['custom_url'] = custom_url
    return analysis


def get_data_export_params(template: str, config: Config, export_root: str):
    default_export_dir = f'{export_root}{config.project}/'
    if template in config.templates:
        templateRec = config.templates[template]
        analysis_key: str = templateRec.analysis_key
        if templateRec.data_path:
            path: str = templateRec.data_path
            if os.path.isdir(path):
                return (path, f'{analysis_key}.json', f'{path}{analysis_key}.json')
            export_dir, file = os.path.split(path)
            return (export_dir, file, None)
        return (default_export_dir, f'{analysis_key}.json', None)

    file_name = f'{template}.json'
    return (default_export_dir, file_name, None)


def export_analysis_details(template: str, config: Config):
    analysis_key = template
    title = analysis_key
    custom_url = None

    if template in config.templates:
        tem_rec = config.templates[template]

        analysis_key = tem_rec.analysis_key
        title = tem_rec.title
        custom_url = tem_rec.custom_url
    return [analysis_key, title, custom_url]


def has_variants_table(cursor):
    cursor.execute("SELECT name FROM sqlite_master WHERE type='table';")
    tables = cursor.fetchall()
    flatten = list(itertools.chain(*tables))
    if 'variants' in flatten:
        return True
    return False


if __name__ == "__main__":
    parser = argparse.ArgumentParser(
        description="Export JSON for visualisation")
    parser.add_argument(
        "-c",
        dest="config",
        default=None,
        help="configuration json file")
    args = parser.parse_args()

    config = validate_config(args.config)

    t0 = time.time()
    os.environ["PYTHONUNBUFFERED"] = "1"

    # connect database
    conn = sqlite3.connect(config.database)
    conn.execute("PRAGMA foreign_keys = 1")
    cur = conn.cursor()

    # create overview tsv and archive of all output
    cur.execute(
        """CREATE TEMP VIEW files AS SELECT trees.*,matrix.* FROM trees LEFT JOIN matrix USING(template);"""
    )
    cur.execute(
        "SELECT template,ctime, mpath, tpath, method, mode from files where method=? order by template;",
        (config.method,),
    )
    rows = cur.fetchall()

    projects = load_projects(
        f"{os.path.realpath(config.export_root)}/projects.json")

    export_root = config.export_root
    project_key = config.project
    has_variants = has_variants_table(cur)

    for row in rows:
        template, datetime, metadata_path, tree_path, method, mode = row

        metadata_path = f'{metadata_path.rsplit(".", 1)[0]}.tsv'
        try:
            data_to_export = load_metadata(metadata_path)
        except FileNotFoundError:
            vu_service.exiting(
                f"post processing: incorrect path for metadata file\n {metadata_path}")

        tree_path = f'{tree_path.rsplit(".", 1)[0]}.nwk'
        try:
            newick = load_newick(tree_path)
        except FileNotFoundError:
            vu_service.exiting(
                "Warning: File not found for {}".format(tree_path))
        data_to_export["newick"] = newick

        # could be simplified into one function
        if has_variants:
            data_to_export['metadata'] = addVariants(
                data_to_export['metadata'], cur)
            data_to_export['columnsDef'].append({'id': 'variants'})

        export_dir, file_name, custom_path = get_data_export_params(
            template, config, export_root)

        Path(f'{export_dir}').mkdir(parents=True, exist_ok=True)
        with open(f'{export_dir}{file_name}', 'w') as data_file:
            json.dump(data_to_export, data_file)

        with gzip.open(f'{export_dir}{file_name}.gz', 'wb') as f_out:
            f_out.write(json.dumps(data_to_export).encode('utf-8'))

        analysis_key, title, custom_url = export_analysis_details(
            template, config)
        # update projects dict
        analysis = createAnalysis(
            'ena', f'{template}_{mode}', title, custom_path, custom_url)

        projects[project_key]['analyses'][analysis_key] = analysis

    with open(f'{export_root}/projects.json', 'w') as projects_file:
        json.dump(projects, projects_file, indent=2)

    conn.close()
    print("DONE", file=sys.stderr)
    sys.exit(0)
