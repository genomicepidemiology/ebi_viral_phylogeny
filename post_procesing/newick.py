def load_newick(path):
    with open(path, "r") as newick_file:
        tree = newick_file.read()
    return tree
