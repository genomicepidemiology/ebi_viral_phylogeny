
from typing import NamedTuple

from collections import namedtuple
import json

AnalysisDef = namedtuple('AnalysisDef', ['data_hub', 'template', 'title', 'data_path', 'custom_url'])
 
def load_projects(projects_path: str):
  with open(projects_path, 'r') as file:
    projects = json.load(file)
    return projects;


def update_projects(projects_path: str, project_key: str, analysis_key: str, analysis: AnalysisDef):
  projects = load_projects(projects_path);
  projects[project_key][analysis_key] = analysis