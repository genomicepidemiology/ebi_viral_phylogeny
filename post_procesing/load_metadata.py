from typing import List
import pandas as pd
import numpy as np
import matplotlib
import matplotlib.cm
from pandas_schema import Column, Schema
from pandas_schema.validation import CanConvertValidation, IsDistinctValidation, IsDtypeValidation, CustomElementValidation
from functools import reduce

import viral_surveillance.vu_service as vu_service

COLOR_MAP = 'plasma'
AUTO_COLOR_COLUMN = 'PANGO-lineage'
COLOR_SKIP_COLUMNS = ['description', 'day', 'region', 'longitude', 'latitude']
COLOR_UNITE_COLUMNS = [['year', 'month'], ['year']]

mandatory_columns = ['id', 'description', 'year', 'month', 'day', 'country__autocolor', 'latitude', 'longitude']

schema = Schema([
    Column('id', [IsDistinctValidation()]),
    Column('description', [], allow_empty=True),
    Column('year', [CanConvertValidation(int)]),
    Column('month', [CanConvertValidation(int)], allow_empty=True),
    Column('day', [CanConvertValidation(int)], allow_empty=True),
    Column('country__autocolor', [], allow_empty=True),
    Column('PANGO-lineage__autocolor', [], allow_empty=True),
    Column('latitude', [IsDtypeValidation(float)], allow_empty=True),
    Column('longitude', [IsDtypeValidation(float)], allow_empty=True),
])

def load_tsv_file(path: str) -> pd.DataFrame:
    df_raw = pd.read_csv(path, delimiter='\t')

    errors = schema.validate(df_raw, columns=mandatory_columns)
    if errors:
        txt_errors = '\n\t'.join(map(str, errors))
        print(txt_errors)
        vu_service.exiting(
            f'tsv metadata file has inccorect format:\n{txt_errors}')

    return df_raw


def transform_metadata(df_raw: pd.DataFrame) -> pd.DataFrame:
    df = df_raw.copy()
    df = df.set_index('id')

    df = df.replace(to_replace='NA', value=None)
    df = df.replace(to_replace='', value=None)
    df = df.where(pd.notnull(df), None)
    if 'year__autocolor' in df:
        del df['year__autocolor']

    df = df.rename(columns={'country__autocolor': 'country'})
    df.year = df['year'].astype(int)
    df.month = df['month'].astype(pd.Int64Dtype())
    df.day = df['day'].astype(pd.Int64Dtype())

    if 'PANGO-lineage__autocolor' in df:
        df = df.rename(columns={'PANGO-lineage__autocolor': 'PANGO-lineage'})

    return df


def split_country_column(df_raw: pd.DataFrame) -> pd.DataFrame:
    df = df_raw.copy()
    df[['country', 'region']] = df_raw['country'].str.split(':', expand=True)
    return df


def change_column_order(df_raw: pd.DataFrame) -> pd.DataFrame:
    cols = df_raw.columns.tolist()
    index_of_country = cols.index('country')
    index_of_region = cols.index('region')
    cols.pop(index_of_region)
    cols.insert(index_of_country+1, 'region')

    df = df_raw[cols]
    return df


def colors_to_unique(df: pd.DataFrame, columns: List[str]) -> dict:
    color_columns = list(df.columns)
    if any(name not in color_columns for name in columns):
        missing = [name for name in columns if name not in color_columns]
        raise ValueError(f'Columns ${missing} are not in dataframee')

    unique_vals = df.groupby(columns, dropna=True).indices.keys()
    gradient = np.linspace(0, 1, len(unique_vals))
    cmap = matplotlib.cm.get_cmap(COLOR_MAP)

    auto_colors = dict()
    for index, value in enumerate(unique_vals):
        color = cmap(gradient[index])
        rgb_hex = matplotlib.colors.to_hex(color)
        if type(value) in (type(float()), np.int64):
            key = int(value)
        elif type(value) == type(tuple()):
            str_value = [str(val) for val in value]
            key = '-'.join(str_value)
        else:
            key = value

        auto_colors[key] = rgb_hex
    return auto_colors


def get_colors(df: pd.DataFrame, skip_columns: List[str] = [], unite_columns: List[List[str]] = []):
    color_columns = list(df.columns)
    for column in skip_columns:
        if column in color_columns:
            color_columns.remove(column)
    for columns in unite_columns:
        for column in columns:
                if column in color_columns:
                    color_columns.remove(column)

    columns_for_unique_color = [[column] for column in color_columns]
    columns_for_unique_color.extend(unite_columns)

    colors = {
        '-'.join(columns): colors_to_unique(df, columns )
        for columns in columns_for_unique_color
    }
    return colors


def transform_df_to_dic(df: pd.DataFrame):
    metadata = df.to_dict(orient='index')
    for values in metadata.values():
        date = {'year': values['year']}
        if pd.notna(values['month']):
            date['month'] = int(values['month'])
        if pd.notna(values['day']):
            date['day'] = int(values['day'])
        del values['year']
        del values['month']
        del values['day']
        values['date'] = date
    return metadata

def get_columns(df: pd.DataFrame):
    df_cols = df.columns.tolist()
    df_cols.remove('year')
    df_cols.remove('month')
    df_cols.remove('day')
    df_cols.remove('longitude')
    df_cols.remove('latitude')
    df_cols.insert(1, 'date')
    columnsIDs = ['id'] + df_cols

    columnsDef = [{'id': id} for id in columnsIDs]
    columnsDef.append({'id': 'longitude', 'hidden': True})
    columnsDef.append({'id': 'latitude', 'hidden': True})
    return columnsDef


def pipe(inital, functions: list):
    def pipe_executor(data, func): return func(data)
    result = reduce(pipe_executor, functions, inital)
    return result


def load_metadata(path: str):
    df = pipe(path, [
        load_tsv_file,
        transform_metadata,
        split_country_column,
        change_column_order,
    ])

    to_export = {
        'columnsDef': get_columns(df),
        'metadata': transform_df_to_dic(df),
        'columnsColors': get_colors(df, COLOR_SKIP_COLUMNS, COLOR_UNITE_COLUMNS),
        'defaultColumn': AUTO_COLOR_COLUMN
    }

    return to_export
