from json import load
from .load_metadata import load_metadata
from .newick import load_newick