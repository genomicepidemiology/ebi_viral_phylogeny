#!/usr/bin/env python3

import sys, os, time
import argparse
import sqlite3
import viral_surveillance.vu_service as vu_service

parser = argparse.ArgumentParser(
    description='Subset a VU database into a new db')
parser.add_argument(
    '-d',
    dest="src_db",
    default=None,
    required=True,
    help='Source VU sample db')
parser.add_argument(
    '-o',
    dest="dest_db",
    default=None,
    required=True,
    help='Destination VU sample db')
parser.add_argument(
    '-q',
    dest="query_file",
    default=None,
    required=True,
    help='Selection query file')
parser.add_argument(
    '-l',
    dest="limit",
    type=int,
    default=10000,
    help='Limit of records transferred')
args = parser.parse_args()

## MAIN
t0 = time.time()
os.environ['PYTHONUNBUFFERED'] = '1'

# check arguments
if not os.path.exists(args.src_db):
    vu_service.exiting("Source database needed")
if not os.path.exists(args.query_file):
    vu_service.exiting("Selection query needed")

selection_queries = []
with open(args.query_file, "r") as fp:
    for line in fp:
        selection_queries.append(line.strip())

dest_conn = sqlite3.connect(args.dest_db)
dest_cur = dest_conn.cursor()
dest_cur.execute('''CREATE TABLE IF NOT EXISTS samples
    (accession TEXT PRIMARY KEY,
    path TEXT,
    feature_path TEXT,
    dl_date TEXT DEFAULT CURRENT_DATE,
    included INTEGER DEFAULT NULL);''')
dest_cur.execute('''CREATE TABLE IF NOT EXISTS metadata
    (accession TEXT PRIMARY KEY,
    description TEXT,
    submit_date TEXT,
    country TEXT);''')
dest_conn.commit()

src_conn = sqlite3.connect(args.src_db)
src_cur = src_conn.cursor()
accession_transfers = []
for query in selection_queries:
    src_cur.execute(query)
    rows = src_cur.fetchall()
    if rows is not None:
        for r in rows:
            accession = r[0]
            dest_cur.execute('''SELECT * FROM samples WHERE accession=?''', (accession,))
            if dest_cur.fetchone() is None:
                accession_transfers.append(accession)

sample_insert = []
metadata_insert = []
for acc in accession_transfers[:args.limit]:
    src_cur.execute('''SELECT accession,path,feature_path,dl_date FROM samples WHERE accession=?''', (acc,))
    record = src_cur.fetchone()
    sample_insert.append(record)
    src_cur.execute('''SELECT accession,description,submit_date,country FROM metadata WHERE accession=?''', (acc,))
    record = src_cur.fetchone()
    metadata_insert.append(record)
src_conn.close()

# insert to db
dest_cur.executemany('''INSERT OR IGNORE INTO samples (accession, path, feature_path, dl_date) VALUES (?,?,?,?);''', sample_insert)
dest_conn.commit()

dest_cur.executemany('''INSERT OR IGNORE INTO metadata (accession, description, submit_date, country) VALUES (?,?,?,?);''', metadata_insert)
dest_conn.commit()

dest_conn.close()


vu_service.timing(t0, f"{min(args.limit, len(accession_transfers))} records transferred")

print("DONE", file=sys.stderr)
sys.exit(0)
