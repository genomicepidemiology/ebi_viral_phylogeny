#!/usr/bin/env python3

import os
import sys
import argparse
import json
import sqlite3
import requests
import shlex
import shutil
import time
import numpy as np
try:
    import cPickle as pickle
except ImportError:
    import pickle

VOL_SIZE = 500

def to_be_processed(d, template, sample):
    if d.get(template) is not None:
        d[template].append(sample)
    else:
        d[template] = [sample]


def remove_sequence_data(filepath):
    if os.path.exists(filepath):
        try:
            os.unlink(filepath)
        except OSError:
            pass
    return

def unpickle(picpath):
    dataobj = None
    if os.path.exists(picpath):
        with open(picpath, "rb") as pp:
            dataobj = pickle.load(pp)
    return dataobj

def repickle(picpath, dataobj):
    with open(picpath, "wb") as picklingfile:
        pickle.dump(dataobj, picklingfile)
    return

def nonred_sample_index(suppress_list, volumeseqs):
    sample_index = []
    for sample in suppress_list:
        try:
            seq_i = volumeseqs.index("{}.fsa".format(sample))
            sample_index.append(seq_i)
        except ValueError:
            # not non-red
            pass
    # reverse it for popping and deleting from matrix
    sample_index.sort(reverse=True)
    return sample_index

def which_vols_to_change(seq_list, volume_size):
    # vol_index[vol_i] = [invol_i_1, invol_i_2]
    vol_index = {}
    for seq_i in seq_list:
        # zero based
        vol_i = seq_i // volume_size
        in_vol_i = seq_i % volume_size
        if vol_index.get(vol_i):
            vol_index[vol_i] += [in_vol_i]
        else:
            vol_index[vol_i] = [in_vol_i]
    return vol_index


parser = argparse.ArgumentParser(
    description='Remove supressed ENA records from the Krummholz pipeline')
parser.add_argument(
    '-b',
    dest="bdir",
    default=None,
    help='Base folder for analysis')
parser.add_argument(
    '-d',
    dest="database",
    default=None,
    help='Sample db')
parser.add_argument(
    '-o',
    dest="ofix",
    default=None,
    help='Optional custom name for analysis folder')
parser.add_argument(
    '-pairwise',
    dest="pw",
    action='store_true',
    help='Pairwise genetic distance calculation')
parser.add_argument(
    '-u',
    dest="url",
    default=None,
    help='ENA API endpoint adress')
parser.add_argument(
    '-j',
    dest="json",
    default=None,
    help='ENA API response like json')
parser.add_argument(
    '-l',
    dest="seqlist",
    default=None,
    help='Optional comma separated list of sequence IDs')
args = parser.parse_args()

# pairwise or all
if args.pw:
    mode = "pw"
else:
    mode = "all"

# paths
args.bdir = os.path.realpath(args.bdir)
ofolder_name = "analysis"
if args.ofix is not None:
    # assumed to be a folder name
    ofolder_name = os.path.basename(args.ofix)
odir = os.path.join(args.bdir, ofolder_name)

# open database
conn = sqlite3.connect(args.database)
conn.execute("PRAGMA foreign_keys = 1")
conn.commit()
cur = conn.cursor()

# optinal list
user_list = []
if args.seqlist is not None:
    user_list = [{'id': x} for x in args.seqlist.split(",")]

# query for supressed records list
suspended_list = []
if args.url is not None:
    response = requests.get(args.url, verify=False)
    if response.status_code == 200:
        suspended_list = response.json().get("results")

offline_suspended_list = []
if args.json is not None:
    offline_suspended_list = json.load(open(args.json, "r")).get("results")
    if suspended_list:
        for item in offline_suspended_list:
            if item not in suspended_list:
                suspended_list.append(item)

# process list of dicts
updated_samples = []
removable = {}
if user_list:
    suspended_list.extend(user_list)

for unit in suspended_list:
    item = unit["id"]
    cur.execute('SELECT path,included from samples where accession=?;', (item,))
    record = cur.fetchall()
    if record:
        record = record[0]
        if record[1] != 403:
            # not removed yet
            updated_samples.append((item,))
            cur.execute('DELETE from metadata where accession=?;', (item,))
            conn.commit()
            remove_sequence_data(record[0])
            cur.execute('SELECT template,qc_pass from templates where accession=?;', (item,))
            rows = cur.fetchall()
            if rows is not None:
                # has template(s)
                later = False
                for r in rows:
                    # delete consensus seq
                    cons_seq = os.path.join(odir, r[0], "{}.fsa".format(item))
                    remove_sequence_data(cons_seq)
                    if r[1] == 1:
                        # included in a tree
                        to_be_processed(removable, r[0], item)

for template in removable.keys():
    cur.execute('CREATE TEMP VIEW templ_seqs AS SELECT sequences.*,curr_template.* from sequences INNER JOIN ( select * from templates where template="{}") AS curr_template ON sequences.db_id = curr_template.db_id;'.format(template))
    conn.commit()

    # stored struct paths
    non_red_pic = os.path.join(odir, template, "non-red.{}.pic".format(mode))
    hr_matrix_npy = os.path.join(odir, template, "hr-matrix.{}.{}.npy".format(mode, "{}"))
    dist_pic_filepath = os.path.join(odir, template, "pairwise.dist.{}.pic".format(mode))
    dist_phylip = os.path.join(odir, template, "dist.{}.mat".format(mode))

    # load pickles
    non_red_seqs = unpickle(non_red_pic)
    distlist = unpickle(dist_pic_filepath)
    # list of list or too large ?
    distmat = None
    # pristine 'all' does not have dist pic
    if distlist is not None and distlist[0][0] == 0:
        distmat = np.array(distlist, dtype=np.uint16)

    # gather volume indeces supressed
    nonredseq_index = nonred_sample_index(removable.get(template), non_red_seqs)
    print(nonredseq_index)

    # clean up non-red seqs
    for seq_i in nonredseq_index:
        non_red_seqs.pop(seq_i)

        # remove from distmatrix
        if distmat is not None:
            distmat = np.delete(np.delete(distmat, seq_i, 0), seq_i, 1)

    # if the phylip needs to be trimmed
    if nonredseq_index and distmat is None and os.path.exists(dist_phylip):
        with open(dist_phylip, "r") as inlip:
            with open(f"{dist_phylip}.tmp", "w") as outlip:
                _ = inlip.readline()
                # new taxano line
                print("  {0}".format(len(non_red_seqs)), file=outlip)
                # zero-based taxa counter
                taxa_i = -1
                for line in inlip:
                    taxa_i += 1
                    # keep line
                    if taxa_i not in nonredseq_index:
                        # remove cols from kept line
                        # re-use taxa id
                        taxa_cols = line.strip().split(None)
                        for taxa_j in nonredseq_index:
                            taxa_cols.pop(taxa_j+1)
                        print("\t".join(taxa_cols), file=outlip)

    # dump new pickles, move tmp phylip
    if nonredseq_index:
        repickle(non_red_pic, non_red_seqs)
        if distmat is not None:
            distlist = distmat.tolist()
            repickle(dist_pic_filepath, distlist)
            # OBS: only remove phylip IF pickled distances exist
            try:
                os.unlink(dist_phylip)
            except IOError:
                print("# Warning: {} not removed".format(dist_phylip))
        elif os.path.exists(dist_phylip):
            try:
                shutil.move("{}.tmp".format(dist_phylip), dist_phylip)
            except OSError as e:
                vu_service.exiting("Phylip matrix cannot move: {}".format(e))

        # remove nonred seqs from npy-s
        vol_index = which_vols_to_change(nonredseq_index, VOL_SIZE)
        #print(vol_index)
        smallest = min(vol_index.keys())
        # old volume volume, zero based
        total_vol = (len(non_red_seqs) + len(nonredseq_index) - 1) // VOL_SIZE
        npy_dest = None
        vol_dest_i = None
        npy_dest_volume_number = smallest
        filled_dest_size = VOL_SIZE
        for vol_source in range(smallest, total_vol + 1):
            # load source from disk, 1 based vol numbers
            src_vol = np.load(hr_matrix_npy.format(vol_source + 1), mmap_mode=None, allow_pickle=True, fix_imports=True)
            # initialize dest volume if not exists
            if npy_dest is None:
                npy_dest = np.zeros((VOL_SIZE, src_vol.shape[1]), dtype=np.int8)
                vol_dest_i = 0
            if vol_index.get(vol_source) is not None:
                for i in range(src_vol.shape[0]):
                    if i not in vol_index.get(vol_source):
                        # keep it
                        npy_dest[vol_dest_i][:] = src_vol[i]
                        vol_dest_i += 1
                        if vol_dest_i == VOL_SIZE:
                            # replace npy_dest_volume_number
                            np.save(hr_matrix_npy.format(npy_dest_volume_number + 1), npy_dest, allow_pickle=True, fix_imports=True)
                            # init new
                            npy_dest = np.zeros((VOL_SIZE, src_vol.shape[1]), dtype=np.int8)
                            vol_dest_i = 0
                            npy_dest_volume_number += 1
                    else:
                        print(vol_source, i)
            else:
                if VOL_SIZE - vol_dest_i < src_vol.shape[0]:
                    # fill open npy_dest
                    npy_dest[vol_dest_i:][:] = src_vol[:VOL_SIZE - vol_dest_i]
                    np.save(hr_matrix_npy.format(npy_dest_volume_number + 1), npy_dest, allow_pickle=True, fix_imports=True)
                    # init new
                    npy_dest = np.zeros((VOL_SIZE, src_vol.shape[1]), dtype=np.int8)
                    npy_dest_volume_number += 1
                    filled_dest_size =  src_vol.shape[0] - VOL_SIZE + vol_dest_i
                    #print(vol_dest_i, src_vol.shape[0], filled_dest_size)
                    npy_dest[:filled_dest_size][:] = src_vol[VOL_SIZE - vol_dest_i:]
                    vol_dest_i = filled_dest_size
                else:
                    # open npy_dest can take the remaining seqs
                    filled_dest_size = vol_dest_i + src_vol.shape[0]
                    #print(vol_dest_i, src_vol.shape[0], filled_dest_size)
                    npy_dest[vol_dest_i:filled_dest_size][:] = src_vol[:]
                    vol_dest_i = filled_dest_size
        # adjust the size of the last volume
        np.save(hr_matrix_npy.format(npy_dest_volume_number + 1), np.delete(npy_dest, np.s_[vol_dest_i::1], 0), allow_pickle=True, fix_imports=True)

        # if fewer volumes than before, delete the excess old ones
        for excess_vol_number in range(npy_dest_volume_number + 1, total_vol + 1):
            shutil.move(hr_matrix_npy.format(excess_vol_number + 1), "{}.del".format(hr_matrix_npy.format(excess_vol_number + 1)))

    for item in removable.get(template):
        cur.execute('SELECT db_id,repr_id from templ_seqs where accession=?;', (item,))
        dbid, reprid = cur.fetchall()[0]
        if reprid is None:
            # reset the clustered samples
            cur.execute('UPDATE sequences set repr_id="N" where repr_id=?;', (item,))
            conn.commit()

        # delete from sequences table
        cur.execute('DELETE from sequences where db_id=?;', (dbid,))
        conn.commit()
    # delete temp sqlite table
    cur.execute('DROP VIEW templ_seqs;')
    conn.commit()

if updated_samples:
    # delete and re-insert into samples
    cur.executemany('DELETE from templates where accession=?;', updated_samples)
    conn.commit()
    cur.executemany('DELETE from samples where accession=?;', updated_samples)
    conn.commit()
    cur.executemany('INSERT into samples(accession,included) values(?,403);', updated_samples)
    conn.commit()

# close database
conn.close()

print("Removed supressed samples: {}".format(" ".join([x[0] for x in updated_samples])))
sys.exit(0)
