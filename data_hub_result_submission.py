#!/usr/bin/env python3

import os
import sys
import re
import argparse
import json
import requests
import shutil
import sqlite3
import time
import viral_surveillance.vu_service as vu_service

def split_recordslist(selected_rows):
    query_limit = 10
    query_batches = []
    query_batches.append([])
    for record in selected_rows:
        query_batches[-1].append(record[0])
        if len(query_batches[-1]) == query_limit:
            query_batches.append([])
    return query_batches

class ENA_portal_search:
    def __init__(self, api_url, auth_required, result_type):
        self.url = api_url
        self.auth = auth_required
        if self.auth and os.environ.get('DCC_USER') is None:
            vu_service.exiting("Error: DCC_USER environmental variable not set")
        self.result_type = result_type
        self.set_doctype()
        self.set_result()
        self.set_access_mode()
        self.set_fields()
        print(self.url)
        #print(self.response.status_code)

    def set_access_mode(self):
        if self.auth:
            # search only the data hub
            if self.url.find("dccDataOnly") == -1:
                self.url = "{}&dccDataOnly=true".format(self.url)
            self.url = self.url.replace("dataPortal=ena", "dataPortal=pathogen")

    def set_doctype(self):
        m = re.search("format=\w+&", self.url)
        if m is not None:
            if m.group(0) != "format=json&":
                # change to the right one
                self.url = self.url.replace(m.group(0), "format=json&")
        else:
            # missing
            tmp = self.url.split("&query=")
            self.url = tmp[0] + "&format=json" + "&query="

    def set_result(self):
        m = re.search("result=\w+&", self.url)
        if m is not None:
            if m.group(0) != "result={}&".format(self.result_type):
                # change to the right one
                self.url = self.url.replace(m.group(0), "result={}&".format(self.result_type))
        else:
            # missing
            tmp = self.url.split("&query=")
            self.url = tmp[0] + "&result={}".format(self.result_type) + "&query="

    def set_fields(self):
        min_fields = ["sample_accession", "study_accession"]
        type_fields = {"analysis": "analysis_accession",
                       "sequence": "accession",
                       "read_experiment": "experiment_accession"}

        # use field type appropriate for record
        self.record_fields = min_fields
        self.record_fields.append(type_fields.get(self.result_type))

        # check the url to contain the minimum fields needed
        if self.url.find("fields=") != -1:
            field_to_add = []
            for field in self.record_fields:
                if self.url.find(field) == -1:
                    field_to_add.append(field)
            if field_to_add:
                tmp = self.url.split("&fields=")
                self.url = tmp[0] + "&fields={},".format("%2C".join(field_to_add)) + tmp[1]
        else:
            # the whole parameter is missing
            tmp = self.url.split("&query=")
            self.url = tmp[0] + "&fields={}".format("%2C".join(self.record_fields)) + "&query="

    def create_query(self, accession_list):
        if self.result_type == "analysis":
            self.query_str = "&analysis_accession=" + "%20OR%20analysis_accession=".join(accession_list)
        else:
            self.query_str = "&accession=" + "%20OR%20accession=".join(accession_list)

    def get_search(self):
        query_url = self.url + self.query_str
        if not self.auth:
            self.response = requests.get(query_url)
        else:
            # authenticated API access
            self.response = requests.get(query_url, auth=(os.environ['DCC_USER'], os.environ['DCC_PWD']))
        if self.response.status_code == 200:
            return self.response.json()
        elif self.response.status_code == 204:
            return []
        else:
            return None


parser = argparse.ArgumentParser(
    description='Collects analysis result to be submitted in the EBI Data hub system')
parser.add_argument(
    '-s','--suffix',
    dest="suffix",
    required=True,
    help='Suffix for outputs, epoch based or custom')
parser.add_argument(
    '-p','--ena-project',
    dest="ena_project",
    required=True,
    help='Project accession for ENA analysis result submission')
parser.add_argument(
    '-o','--odir',
    dest="odir",
    required=True,
    help='Analysis folder path')
parser.add_argument(
    '-d','--database',
    dest="database",
    required=True,
    help="Sample database")
parser.add_argument(
    '-u','--url',
    dest="api_url",
    default="https://www.ebi.ac.uk/ena/portal/api/search?dataPortal=ena&result=analysis&fields=sample_accession%2Cstudy_accession&query=",
    help='Url for Portal API search')
parser.add_argument(
    '-a','--authenticate',
    dest="auth",
    action="store_true",
    help="Access private data hub data, credentials supplied in DCC_USER and DCC_PWD env. variables"
)
parser.add_argument(
    '-b','--bundle',
    dest="bundle",
    action="store_true",
    help="Bundle files into a tar and prepare config json for internal submission"
)
parser.add_argument(
    '-y','--config',
    dest="sub_y",
    help="Path of submission config yaml for the pipeline"
)
args = parser.parse_args()

if os.path.exists(args.odir) and os.path.isdir(args.odir):
    old_dir = os.getcwd()
    os.chdir(args.odir)
    file_prefix = f"phylogenies_{args.suffix}"
    if args.bundle:
        subm_folder = f"ena-submission_{args.suffix}"
        os.mkdir(subm_folder)
        # place files into subm_folder
        if args.sub_y is not None and os.path.exists(args.sub_y):
            shutil.copy(args.sub_y, f"{subm_folder}/")
        else:
            ena_submitter_folder = os.path.dirname(shutil.which("analysis_submission.py"))
            shutil.copy(os.path.join(ena_submitter_folder, "config.yaml"), f"{subm_folder}/")
        shutil.move(f"{file_prefix}.tar.gz", f"{subm_folder}/")
        if os.path.exists(f"{file_prefix}.tsv"):
            shutil.move(f"{file_prefix}.tsv", f"{subm_folder}/")
        file_prefix = os.path.join(subm_folder, f"phylogenies_{args.suffix}")

    # make a list of files to be submitted
    submitted_files = f"{file_prefix}.tar.gz"
    if os.path.exists(f"{file_prefix}.tsv"):
        submitted_files = f"{file_prefix}.tsv.gz,{file_prefix}.tar.gz"
        # gzip the tsv file too if exists
        if not os.path.exists(f"{file_prefix}.tsv.gz"):
            exit_code = vu_service.call_cmd(f"gzip {file_prefix}.tsv")
            if exit_code:
                vu_service.exiting("Error: Zipping error on tsv.")

    # make a list of the contained runs
    conn = sqlite3.connect(args.database)
    conn.execute("PRAGMA foreign_keys = 1")
    cur = conn.cursor()

    connected_type = "run"
    record_type = "run"
    sample_list_file = f"{file_prefix}.lst"
    ofile = open(sample_list_file, "w")
    cur.execute('''SELECT accession from samples where included=1;''')
    rows = cur.fetchall()
    if rows:
        # match record type
        if re.match("[SED]RR", rows[0][0]) is not None:
            # run type
            for record in rows:
                print(record[0], file=ofile)
        elif re.match("[SED]RZ", rows[0][0]) is not None:
            # analysis type
            connected_type = "sample"
            record_type = "analysis"
        else:
            # sequence type
            connected_type = "sample"
            record_type = "sequence"

        if connected_type == "sample":
            rec_id_batch = split_recordslist(rows)
            ena_conn = ENA_portal_search(args.api_url, args.auth, record_type)
            for batch in rec_id_batch:
                ena_conn.create_query(batch)
                reply_obj = ena_conn.get_search()
                sleeps = 0
                while reply_obj is None and sleeps < 5:
                    time.sleep(1)
                    sleeps += 1
                    reply_obj = ena_conn.get_search()
                if reply_obj is None and sleeps == 5:
                    vu_service.exiting("Error: not connecting to ENA")
                for rec in reply_obj:
                    if rec.get('sample_accession') is not None and rec.get('sample_accession'):
                        print(rec.get('sample_accession'), file=ofile)

    conn.close()
    ofile.close()

if args.bundle:
    # bundle has tar.gz, tsv.gz, config.yaml, lst
    # tar it
    exit_code = vu_service.call_cmd(f"tar -cf {subm_folder}.tar {subm_folder}/")
    if exit_code:
        vu_service.exiting("Tarring error.")
    subm_args = {
    "PROJECT_ACCESSION": args.ena_project,
    "SAMPLE_ACCESSION": "",
    "RUN_ACCESSION": "",
    "FILE_NAME": submitted_files,
    "ANALYSIS_TYPE": "PHYLOGENY_ANALYSIS"
    }
    if connected_type == "sample":
        subm_args["SAMPLE_ACCESSION"] = sample_list_file
    else:
        subm_args["RUN_ACCESSION"] = sample_list_file
    with open(f"{subm_folder}.json", "w") as op:
        json.dump(subm_args, op)
    try:
        shutil.rmtree(subm_folder)
    except:
        vu_service.exiting("Submission bundle folder couldn't be removed")
    sys.exit()

elif not args.bundle and shutil.which('analysis_submission.py') is not None:
    ena_submitter_folder = os.path.dirname(shutil.which("analysis_submission.py"))
    cmd = f"analysis_submission.py -p {args.ena_project} -r {sample_list_file} -f {submitted_files} -o {args.odir} -a PHYLOGENY_ANALYSIS "
    if connected_type == "sample":
        cmd = f"analysis_submission.py -p {args.ena_project} -s {sample_list_file} -f {submitted_files} -o {args.odir} -a PHYLOGENY_ANALYSIS "
    cmd += "-au ${WEBIN_USER} -ap ${WEBIN_PWD} "
    cmd += "-t F"
    exit_code = vu_service.call_shell(cmd)
    if exit_code:
        vu_service.exiting("Error: ENA analysis submission failed")
    else:
        try:
            os.unlink(sample_list_file)
        except OSError as e:
            vu_service.exiting(f"Warning: Analysis result submitted, but {sample_list_file} couldn't be removed")
        else:
            sys.exit()
else:
    vu_service.exiting("Error: Submission not finished")
